CREATE TABLE parameters (
  name TEXT PRIMARY KEY NOT NULL,
  value NUMERIC NOT NULL);

CREATE TABLE nodes (
  id INTEGER NOT NULL,
  time NUMERIC NOT NULL,
  state TEXT NOT NULL,
  PRIMARY KEY (id, time));

CREATE TABLE edges (
  node_1 INTEGER NOT NULL, 
  node_2 INTEGER NOT NULL,
  FOREIGN KEY (node_1) REFERENCES nodes(id),
  FOREIGN KEY (node_2) REFERENCES nodes(id),
  UNIQUE (node_1, node_2));

CREATE TABLE attributes (
  node INTEGER NOT NULL,
  time NUMERIC NOT NULL,
  name TEXT NOT NULL,
  value NUMERIC,
  PRIMARY KEY (node, time, name),
  FOREIGN KEY (node) REFERENCES nodes(id));
