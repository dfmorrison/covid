# Notes SEIRD model with varying beta rate. The transmission occurs with rate β when a node has at least one Infected
#  neighbor. We will update beta GLOBALLY

import networkx as nx
import random
# import heapq
import numpy as np
import EoN
from collections import defaultdict
from collections import Counter
import matplotlib.pyplot as plt

# rng = np.random.RandomState(123456)
# random.seed(rng)
# np.random.seed(rng)

# from matplotlib.animation import FuncAnimation
# import matplotlib.animation
# from matplotlib import rc # YOU HAVE TO IMPORT AND SPECIFY THE RC
# rc('animation', html='html5')
# import community
from datetime import datetime

class beta_model():
    def __init__(self, params):
        self.beta0 = params[0]
        self.beta = self.beta0
        self.beta_old = 0.5
        self.mu = params[1]
        self.gamma = params[2]
        self.frac_D = params[3]
        # self.M1 = self.beta0
        self.M0 = params[4]
        self.eps = params[5]
        # self.Dcrit_range = 10^-5*[.01 0.05  0.1 0.5];
        self.Dcrit = params[6] # δc
        # self.Dtot_crit_range = [1000 2000 5000]/self.N;
        self.Dtot_crit = params[7]
        self.awareness = params[8]
        # self.gamma_H = params[9]
        self.Dday_old = 0


    def update(self, d):
        # δ = Dday :param is the daily deaths
        # Dc is the cumulative or long term deaths.
        # d = D total death count
        # self.eps = 0.1 # DELETE
        self.Dday = d-self.Dday_old# how many new nodes are dead #self.gamma_H*h;

        self.beta = 0.5*self.eps*( (self.beta0/(1+(self.Dday/self.Dcrit)**self.awareness)) - self.beta)*\
                1/(1+(d/self.Dtot_crit)**self.awareness) + 0.5*self.eps*(self.beta0-self.beta) + self.beta

        self.Dday_old = self.Dday
        # self.beta_old = self.beta #Notes: We start with self.beta in init and then we update so no need for beta_old

        return self.beta # this should be applied to the whole network


class _ListDict_(object):
    r'''
    The Gillespie algorithm will involve a step that samples a random element
    from a set based on its weight.  This is awkward in Python.

    So I'm introducing a new class based on a stack overflow answer by
    Amber (http://stackoverflow.com/users/148870/amber)
    for a question by
    tba (http://stackoverflow.com/users/46521/tba)
    found at
    http://stackoverflow.com/a/15993515/2966723

    This will allow me to select a random element uniformly, and then use
    rejection sampling to make sure it's been selected with the appropriate
    weight.

    I believe a faster data structure can be created with a (binary) tree.
    We add an object with a weight to the tree.  The nodes track their weights
    and the sum of the weights below it.  So choosing a random object (by weight)
    means that we choose a random number between 0 and weight_sum.  Then
    if it's less than the first node's weight, we choose that.  Otherwise,
    we see if the remaining bit is less than the total under the first child.
    If so, go there, otherwise, it's the other child.  Then iterate.  Adding
    a node would probably involve placing higher weight nodes higher in
    the tree.  Currently I don't have a fast enough implementation of this
    for my purposes.  So for now I'm sticking to the mixture of lists &
    dictionaries.

    I believe this structure I'm describing is similar to a "partial sum tree"
    or a "Fenwick tree", but they seem subtly different from this.
    '''

    def __init__(self, weighted=False):
        self.item_to_position = {}
        self.items = []

        self.weighted = weighted
        if self.weighted:
            self.weight = defaultdict(int)  # presume all weights positive
            self.max_weight = 0
            self._total_weight = 0
            self.max_weight_count = 0

    def __len__(self):
        return len(self.items)

    def __contains__(self, item):
        return item in self.item_to_position

    def _update_max_weight(self):
        C = Counter(self.weight.values())  # may be a faster way to do this, we only need to count the max.
        self.max_weight = max(C.keys())
        self.max_weight_count = C[self.max_weight]

    def insert(self, item, weight=None):
        r'''
        If not present, then inserts the thing (with weight if appropriate)
        if already there, replaces the weight unless weight is 0

        If weight is 0, then it removes the item and doesn't replace.

        WARNING:
            replaces weight if already present, does not increment weight.


        '''
        if self.__contains__(item):
            self.remove(item)
        if weight != 0:
            self.update(item, weight_increment=weight)

    def update(self, item, weight_increment=None):
        r'''
        If not present, then inserts the thing (with weight if appropriate)
        if already there, increments weight

        WARNING:
            increments weight if already present, cannot overwrite weight.
        '''
        if weight_increment is not None:  # will break if passing a weight to unweighted case
            if weight_increment > 0 or self.weight[item] != self.max_weight:
                self.weight[item] = self.weight[item] + weight_increment
                self._total_weight += weight_increment
                if self.weight[item] > self.max_weight:
                    self.max_weight_count = 1
                    self.max_weight = self.weight[item]
                elif self.weight[item] == self.max_weight:
                    self.max_weight_count += 1
            else:  # it's a negative increment and was at max
                self.max_weight_count -= 1
                self.weight[item] = self.weight[item] + weight_increment
                self._total_weight += weight_increment
                self.max_weight_count -= 1
                if self.max_weight_count == 0:
                    self._update_max_weight
        elif self.weighted:
            raise Exception('if weighted, must assign weight_increment')

        if item in self:  # we've already got it, do nothing else
            return
        self.items.append(item)
        self.item_to_position[item] = len(self.items) - 1

    def remove(self, choice):
        position = self.item_to_position.pop(choice)
        last_item = self.items.pop()
        if position != len(self.items):
            self.items[position] = last_item
            self.item_to_position[last_item] = position

        if self.weighted:
            weight = self.weight.pop(choice)
            self._total_weight -= weight
            if weight == self.max_weight:
                # if we find ourselves in this case often
                # it may be better just to let max_weight be the
                # largest weight *ever* encountered, even if all remaining weights are less
                #
                self.max_weight_count -= 1
                if self.max_weight_count == 0 and len(self) > 0:
                    self._update_max_weight()

    def choose_random(self):
        # r'''chooses a random node.  If there is a weight, it will use rejection
        # sampling to choose a random node until it succeeds'''
        if self.weighted:
            while True:
                choice = random.choice(self.items)
                if random.random() < self.weight[choice] / self.max_weight:
                    break
            # r = random.random()*self.total_weight
            # for item in self.items:
            #     r-= self.weight[item]
            #     if r<0:
            #         break
            return choice

        else:
            return random.choice(self.items)

    def random_removal(self):
        r'''uses other class methods to choose and then remove a random node'''
        choice = self.choose_random()
        self.remove(choice)
        return choice

    def total_weight(self):
        if self.weighted:
            return self._total_weight
        else:
            return len(self)

    def update_total_weight(self):
        self._total_weight = sum(self.weight[item] for item in self.items)


def Gillespie_complex_contagion(G, rate_function, transition_choice,
                                get_influence_set, IC, return_statuses, tmin = 0, tmax=100, parameters = None,
                                return_full_data = False, sim_kwargs = None):

    r'''
    Initially intended for a complex contagion.  However, this can allow influence
    from any nodes, not just immediate neighbors.

    The complex contagion must be something that all nodes do something simultaneously

    **This is not the same as if node** ``v`` **primes node** ``u`` **and
    later node** ``w`` **causes** ``u`` **to transition.  This will require
    that both** ``v`` **and** ``w`` **have the relevant states at the moment**
    **of transition and it has forgotten any previous history.**

    :Arguments:

    **G** (NetworkX Graph)
        The underlying network

    **rate_function**
        A function that will take the network, a node, and the statuses of all
        the nodes and calculate the rate at which the node changes its status.
        The function is called like

        if parameters is None:
            rate_function(G, node, status)
        else:
            rate_function(G, node, status, parameters)

        where G is the graph, node is the node, status is a dict such that
        status[u] returns the status of u, and parameters is the parameters
        passed to the function.

        it returns a number, the combined rate at which ``node`` might change
        status.


    **transition_choice**
        A function that takes the network, a node, and the statuses of all the
        nodes and chooses which event will happen.  The function should be
        called [with or without ``parameters``]

        if ``parameters`` is ``None``:
            ``transition_choice(G, node, status)``
        else:
            ``transition_choice(G, node, status, parameters)``

        where ``G`` is the graph, ``node` is the node, ``status` is a dict
        such that ``status[u]`` returns the status of u, and ``parameters`` is
        the parameters passed to the function.

        It should return the new status of ``node`` based on the fact that the
        node is changing status.

    **get_influence_set**
        When a node ``u`` changes status, we want to know which nodes may have their
        rate altered.  We need to update their rates.  This function returns all
        nodes that may be affected by ``u`` (either in its previous state or its
        current state).  We will go through and recalculate the rates for all
        of these nodes.  For a contagion, we can simply choose all neighbors,
        but it may be faster to leave out any nodes that it wouldn't have
        affected before or after its transition (e.g., R or I neighbors in SIR).

        if ``parameters`` is None:
            ``get_influence_set(G, node, status)``
        else:
            ``get_influence_set(G, node, status, parameters)``

        where G is the graph, node is the node, status is a dict such that
        status[u] returns the status of u, and parameters is the parameters
        passed to the function.

        it should return the set of nodes whose rates need to be recalculated.

        Most likely, it is

        def get_influence_set(G, node, status):
            return G.neighbors(node)


    **IC**
        A dict.  IC[node] returns the status of node.

    **return_statuses** list or other iterable (but not a generator)
        The statuses that we will return information for, in the order
        we will return them.

    **tmin** number (default 0)
        starting time

    **tmax** number (default 100)
        stop time

    **return_full_data** boolean
        currently needs to be False.  True raises an error.

    **parameters**  list/tuple.
        Any parameters of the functions rate_function, transition_choice, influence_set
        We assume all three functions can accept parameters.
        Examples: recovery rate, transmission rate, ...


    :Returns:
    **(times, status1, status2, ...)**  tuple of numpy arrays
        first entry is the times at which events happen.
        second (etc) entry is an array with the same number of entries as ``times``
        giving the number of nodes of status ordered as they are in ``return_statuses``

    :SAMPLE USE:
    This simply does an SIR epidemic, by saying that the rate of becoming
    infected is tau times the number of infected neighbors.

    ::
        import networkx as nx
        import EoN
        import matplotlib.pyplot as plt
        from collections import defaultdict #makes defining the initial condition easier


        def rate_function(G, node, status, parameters):
            #This function needs to return the rate at which node changes status.
            #
            tau,gamma = parameters
            if status[node] == 'I':
                return gamma
            elif status[node] == 'S':
                return tau*len([nbr for nbr in G.neighbors(node) if status[nbr] == 'I'])
            else:
                return 0

        def transition_choice(G, node, status, parameters):
            #this function needs to return the new status of node.  We already
            #know it is changing status.
            #
            #this function could be more elaborate if there were different
            #possible transitions that could happen.
            if status[node] == 'I':
                return 'R'
            elif status[node] == 'S':
                return 'I'

        def get_influence_set(G, node, status, parameters):
            #this function needs to return any node whose rates might change
            #because ``node`` has just changed status.
            #
            #the only neighbors a node might affect are the susceptible ones.

            return {nbr for nbr in G.neighbors(node) if status[nbr] == 'S'}

        G = nx.fast_gnp_random_graph(100000,0.00005)

        gamma = 1.
        tau = 0.5
        parameters = (tau, gamma)

        IC = defaultdict(lambda: 'S')
        for node in range(200):
            IC[node] = 'I'

        t, S, I, R = EoN.Gillespie_complex_contagion(G, rate_function,
                                   transition_choice, get_influence_set, IC,
                                   return_statuses=('S', 'I', 'R'),
                                   parameters=parameters)

        plt.plot(t, I)

    '''

    if parameters is None:
        parameters = ()


    status = {node: IC[node] for node in G.nodes()}

    if return_full_data:
        node_history = {node :([tmin], [status[node]]) for node in G.nodes()}

    times = [tmin]
    t = tmin
    data = {}
    C = Counter(status.values())
    for return_status in return_statuses:
        data[return_status] = [C[return_status]]

    nodes_by_rate = _ListDict_(weighted=True)

    for u in G.nodes():
        rate = rate_function(G, u, status, parameters) # rate is specified by a function (initialization)!
        if rate >0:
            nodes_by_rate.insert(u, weight = rate)

    if nodes_by_rate.total_weight( ) >0:
        delay = random.expovariate(nodes_by_rate.total_weight())
    else:
        delay = float('Inf')
    t += delay
    attitude = [0.0]
    beta_list = []
    dead_list = []
    time_list = []
    i=0
    b=True
    betamodel = beta_model(parameters)
    while nodes_by_rate.total_weight( ) > 0 and t< tmax:
        print('===== Time:',t)
        if i % 300 == 0:#b==True:#i % 200 == 0: # or 200 % t == 0 # 50 is good when you use neighbors for contagion
            print('Update nodes beliefs') #TODO: We should update every 7 days
            # We will update globally so all nodes will get same beta.
            statuses = ['S', 'E', 'I', 'R', 'D']
            d = {}
            Counts = Counter(status.values()) # Get S,I,R counts. It gives only S,I as R at least at the beginning is
            # empty
            for return_status in statuses:
                d[return_status] = [Counts[return_status]]

            beta_t = betamodel.update(d['D'][0])
            beta_list.append(beta_t)
            dead_list.append(betamodel.Dday)
            time_list.append(t)
            print('behavior=',beta_t/0.5)
            betas = {node: beta_t for node in G}
            nx.set_node_attributes(G, values=betas, name='beta')

            # for node in G:
            #     inf_neighs = len([nbr for nbr in G.neighbors(node) if status[nbr] == 'I']) #/ len([nbr for nbr in G.neighbors(node)])
            #     # new_belief = np.minimum(1.0, G.nodes[node]['attitude'].run_period(d['S'][0], d['I'][0], d['R'][0]))
            #     new_belief = G.nodes[node]['attitude'].run_period(inf_neighs)
            #     G.nodes[node]['masked'] = new_belief # Belief can't go beyond 1.0
        i+=1
        #avg_att = np.mean([G.nodes[nbr]['masked'] for nbr in G]) # takes too much time cauz it runs over all nodes!
        #attitude.append(avg_att)#G.nodes[46,46]['masked'])
        # print(avg_att)
        times.append(t)
        #        print(delta_t, nodes_by_rate.total_weight())
        node = nodes_by_rate.choose_random()
        new_status = transition_choice(G, node, status, parameters)

        for x in data.keys():
            data[x].append(data[x][-1])
        if status[node] in return_statuses:
            data[status[node]][-1] -= 1
        if new_status in return_statuses:
            data[new_status][-1] += 1

        status[node] = new_status
        if return_full_data:
            node_history[node][0].append(t)
            node_history[node][1].append(new_status)

        # update self
        weight = rate_function(G, node, status, parameters) # rate update!
        nodes_by_rate.insert(node, weight=weight)

        influence_set = get_influence_set(G, node, status, parameters)

        for nbr in influence_set:
            weight = rate_function(G, nbr, status, parameters) # rate update for the nodes that need to get updated!
            nodes_by_rate.insert(nbr, weight=weight)

        if nodes_by_rate.total_weight() > 0:
            delay = random.expovariate(nodes_by_rate.total_weight())
        else:
            delay = float('Inf')
        t += delay

    plt.plot(np.array(beta_list) / betamodel.beta0, np.array(dead_list), color='purple')
    plt.show()
    plt.semilogy(np.array(beta_list) / betamodel.beta0, np.array(dead_list), color='purple')
    plt.show()
    print(len(times))
    if not return_full_data:
        returnval = []
        times = np.array(times)
        returnval.append(times)
        for return_status in return_statuses:
            data[return_status] = np.array(data[return_status])
            returnval.append(data[return_status])
        return returnval
    else:
        if sim_kwargs is None:
            sim_kwargs = {}
        return EoN.Simulation_Investigation(G, node_history, possible_statuses=return_statuses, **sim_kwargs)


def main(**kwargs):
    # :SAMPLE USE:
    # This simply does an SIR epidemic, by saying that the rate of becoming
    # infected is tau times the number of infected neighbors.

    def rate_function(G, node, status, parameters): # It runs initially with init conditions and then in the sim loop
        # This function needs to return the rate at which node changes status.
        # For SEIR the only NON-spontaneous transition is from S-->E
        beta, mu, gamma, frac_D, M0, eps, Dcrit, Dtot_crit, awareness = parameters
        if status[node] == 'I': # I --> R, D
            return gamma
        elif status[node] == 'S': # S --> E
            r = len([nbr for nbr in G.neighbors(node) if (status[nbr] == 'I')]) # Only infected can affect
            # susceptible and then these they become Exposed
            if r >= 1: # at least one neighbor infected
                return G.nodes[node]['beta']#beta
            else:
                return 0
        elif status[node] == 'E': # E --> I
            return mu
        else:
            return 0

    def transition_choice(G, node, status, parameters):
        # this function needs to return the new status of node.  We already
        # know it is changing status.
        #
        # this function could be more elaborate if there were different
        # possible transitions that could happen.
        # HERE WE NEED TO UPDATE BELIEFS!!! A S node will be visited twice (S->I->R)
        # NO WE NEED TO UPDATE BELIEFS AT EVERY TIMESTEP IN GILLESPIE
        # print(node)
        beta, mu, gamma, frac_D, M0, eps, Dcrit, Dtot_crit, awareness = parameters

        if status[node] == 'I':
            if random.random() < frac_D:  # random.random() < epsilon: # rng.random_sample()
                # print('EXPLORE')
                # self.rng.choice(range(self.num_actions), nenvs)
                return 'D'
            else:
                return 'R'
        elif status[node] == 'S':
            return 'E'
        elif status[node] == 'E':
            return 'I'

    def get_influence_set(G, node, status, parameters):
        # this function needs to return any node whose rates might change
        # because ``node`` has just changed status.
        #
        # the only neighbors a node might affect are the susceptible ones.

        # However it can affect neighbors ONLY if that node CAN actually transmit something. Check if you need to put
        # an if cond here (if node' status== I then return the S neighbors else return empty)
        if status[node] == 'I':
            return {nbr for nbr in G.neighbors(node) if status[nbr] == 'S'}
        else:
            return {}

    now = datetime.now()
    start_time = now.strftime("%H:%M:%S")

    tmax = 400
    N = 10**4#6#4#7, ^6 2mins total simulation.
    beta = 0.5
    mu = 1/2
    gamma = 1/6
    frac_D=0.01
    M1=beta
    M0=0.15
    eps=1/7
    # self.Dcrit_range = 10^-5*[.01 0.05  0.1 0.5];
    Dcrit = 0.05*(10**(-5)) #<--CHANGED
    # self.Dtot_crit_range = [1000 2000 5000]/self.N;
    Dtot_crit = 25#5000/N <--CHANGED
    awareness = 2
    gamma_H=1/21

    # rng = 1#np.random.RandomState(123456) # Random state doesnt seem to work
    # random.seed(rng)
    parameters = (beta, mu, gamma, frac_D, M0, eps, Dcrit, Dtot_crit, awareness)
    for simulation in range(1):
        rng = 1  # np.random.RandomState(123456) # Random state doesnt seem to work
        random.seed(rng)
        print('|☛ Create Network')
        G = nx.barabasi_albert_graph(N, 1, seed=rng) # Barabasi (exhibits power laws like in real
        # world). If the parameter is 1 it creates quite realistic networks with influencers or city-like maps.
        # G = nx.fast_gnp_random_graph(N, 5. / (N - 1)) # Erdős-Rényi (random networks)
        # G = nx.grid_2d_graph(100, 100)  # each node is (u,v) where 0<=u,v<=99, SUPER FAST SIMULATION

        # INFECTIONS ARE PLACED IN THE MIDDLE in the Grid network!
        # init_infected = [(u, v) for (u, v) in G if 45 < u < 55 and 45 < v < 55]

        # Non grid network
        init = 1
        # random.seed(rng)
        init_infected = random.sample(list(G.nodes()), init)  # init=1 (num of initial infections)
        print('Init infected', init_infected)

        betas = {node: beta for node in G}
        nx.set_node_attributes(G, values=betas, name='beta')

        IC = defaultdict(lambda: 'S')
        for node in init_infected:
            IC[node] = 'I' # for N=100 and 45<x,y<55 there are 81 nodes initial Infected

        # ANIMATION
        color_dict = {'S': '#009a80', 'E': 'orange', 'I': '#ff2020', 'R': 'blue', 'D': 'gray'}  # ,'Vac': '#5AB3E6'}
        pos = {node: node for node in G}
        tex = False
        sim_kwargs = {'color_dict': color_dict, 'pos': pos}
                      #'tex': tex}  # sim doesnt work if we include these. By default SIR is assumed.
        print('|☛ Start Gillespie')
        sim = Gillespie_complex_contagion(G, rate_function, # use EoN.Gillespie... to use the original function
                                                     transition_choice, get_influence_set, IC, return_full_data=True,
                                                     tmax = tmax,
                                                     sim_kwargs=sim_kwargs,
                                                     return_statuses=('S', 'E', 'I', 'R', 'D'),
                                                     parameters=parameters)


        # Simulation ended
        end = datetime.now()
        finish_time = end.strftime("%H:%M:%S")
        print("Time Start Simulation =", start_time)
        print("Time Finish Simulation =", finish_time)
        times, D = sim.summary()
        newD = {'S': D['S'], 'E': D['E'], 'I': D['I'], 'R': D['R'], 'D': D['D']}

        new_timeseries = (times, newD)
        sim.add_timeseries(new_timeseries, color_dict={'S': '#009a80', 'E': 'orange', 'I': '#ff2020', 'R': 'blue',
                                                       'D': 'gray'})

        nx_kwargs = dict(edgecolors='black', node_size=4, width=0.0) # for video
        print('Creating Video')
        # ani = sim.animate(ts_plots=[['S', 'E', 'I', 'R', 'D']], **nx_kwargs)
        # ani.save('seird.mp4', fps=5, extra_args=['-vcodec', 'libx264'])

        print('Video Saved')
        # plt.plot(times, D['S'], label='Susceptible', color='#009a80')
        plt.plot(times, D['E'], label='Exposed', color='orange')
        plt.plot(times, D['I'], label='Infected', color='#ff2020')
        # plt.plot(times, D['R'], label='Recovered', color='blue')
        plt.plot(times, D['D'], label='Dead', color='gray')
    plt.legend()
    plt.show()

    print('Max Infected=', D['I'].max())
    print('Max Dead=', D['D'].max())

if __name__ == "__main__":
    main()

# NOTES:
# The way it is you can just run IBL multiple times with the rate of one timestep and put this value to the masked
# attribute. We do it like this in case we run interactive stuff and input is a neighboring property.