# IBL estimates a decision that is closer to the (weighted) past decisions
import networkx as nx
import random
# import heapq
import numpy as np
import EoN
from collections import defaultdict
from collections import Counter
import matplotlib.pyplot as plt

import click
import dataclasses
from itertools import count
# import logging
import math
import pyactup
# from matplotlib.animation import FuncAnimation
# import matplotlib.animation
# from matplotlib import rc # YOU HAVE TO IMPORT AND SPECIFY THE RC
# rc('animation', html='html5')
# import community
from datetime import datetime


DEFAULT_SUSCEPTIBLE = 9_919
DEFAULT_INFECTIOUS = 81
DEFAULT_RECOVERED = 0
DEFAULT_NOISE = 0.01
DEFAULT_TEMPERATUE = 1.0
DEFAULT_MISMATCH = 1.0


@dataclasses.dataclass
class State:
    susceptible: int = DEFAULT_SUSCEPTIBLE
    infectious: int = DEFAULT_INFECTIOUS
    recovered: int = DEFAULT_RECOVERED

    @property
    def population(self):
        return self.susceptible + self.infectious + self.recovered

    @property
    def infection_rate(self):
        return self.infectious / self.population

    def sir(self, S, I, R, r0=1.0, periods=1):
        # logging.debug("sir called: %s %s", self, r0)
        for i in range(periods):
            rt = r0 * self.susceptible / self.population
            # The new S,I,R populations
            self.recovered = R #+= self.infectious
            self.infectious = I #min(math.floor(self.infectious * rt), self.susceptible)
            self.susceptible = S #-= self.infectious
        else:
            rt = r0
        # logging.debug("sir result = %s", rt)
        return rt

def mask_rt_mapping(probability, r0=2.0, mask_factor=2.0):
    return math.pow(mask_factor, mask_factor * (0.5 - probability)) * r0 / mask_factor

SIMILARITY_RANGE = 4#2

def linear_similarity(x, y):
    if x is None or y is None:
        return 1
    else:
        result = 1 - abs(y - x) / SIMILARITY_RANGE
        assert result >= 0 and result <= 1
        return result

pyactup.set_similarity_function(linear_similarity, "input")


class MaskWearer(pyactup.Memory):

    def __init__(self,
                 wear=1, dont_wear=1, fear=1, hope=1,
                 r0=2.0, mask_factor=2.0, rate_factor=0.5,
                 susceptible=DEFAULT_SUSCEPTIBLE,
                 infectious=DEFAULT_INFECTIOUS,
                 recovered=DEFAULT_RECOVERED,
                 **mem_params):
        if "noise" not in mem_params:
            mem_params["noise"] = DEFAULT_NOISE
        if "temperature" not in mem_params:
            mem_params["temperature"] = DEFAULT_TEMPERATUE
        if "mismatch" not in mem_params:
            mem_params["mismatch"] = DEFAULT_MISMATCH
        super().__init__(**mem_params)
        self.learning_time_increment = 0
        self.retrieval_time_increment = 1
        self.state = State(susceptible, infectious, recovered)
        self.r0 = r0
        self.rate = r0
        self.mask_factor = mask_factor
        self.rate_factor = rate_factor
        # put the extreme values
        for n, i, a in ((wear, None, 1.0), #
                        (dont_wear, None, 0.0),
                        (fear, 4.0, 1.0), # rate=4 so then wear mask with prob 1.0
                        (hope, 0.0, 0.0)): # rate=0 (no infections) no mask wearing
            for j in range(n):
                self.learn(input=i, action=a) # Here we put in the memory these 4 chunks. Then model interpolates in
                # just these 4 chunks responses.
        # logging.debug("initialized %s (%s, %d, %d, %d, %d)",
        #               self, self.state, wear, dont_wear, fear, hope)

    def mask_rt_mapping(self, probability):
        return (math.pow(self.mask_factor, self.mask_factor * (0.5 - probability)) # mf^( mf*(.5-prob) )
                * self.r0 / self.mask_factor)

    def run_period(self, r_neighs):
        self.rate = r_neighs
        # if self.rate <= 0:
        #     return
        assert self.rate <= SIMILARITY_RANGE
        action = self.blend("action", input=self.rate)
        result = dataclasses.asdict(self.state)
        result["rate"] = self.rate
        # print(self.rate)
        # new_rate = self.state.sir(S, I, R, mask_rt_mapping(action)) # S,I,R populations are getting updated here
        # outcome = (self.state.infection_rate - new_rate) / new_rate
        # Below: Each node has its own "rate" or perception of the pandemic
        # self.rate = r_neighs#self.rate_factor * self.rate + (1.0 - self.rate_factor) * new_rate
        # logging.debug("ran period on %s (%f) => %s", self, self.rate, result)
        return action


class _ListDict_(object):
    r'''
    The Gillespie algorithm will involve a step that samples a random element
    from a set based on its weight.  This is awkward in Python.

    So I'm introducing a new class based on a stack overflow answer by
    Amber (http://stackoverflow.com/users/148870/amber)
    for a question by
    tba (http://stackoverflow.com/users/46521/tba)
    found at
    http://stackoverflow.com/a/15993515/2966723

    This will allow me to select a random element uniformly, and then use
    rejection sampling to make sure it's been selected with the appropriate
    weight.

    I believe a faster data structure can be created with a (binary) tree.
    We add an object with a weight to the tree.  The nodes track their weights
    and the sum of the weights below it.  So choosing a random object (by weight)
    means that we choose a random number between 0 and weight_sum.  Then
    if it's less than the first node's weight, we choose that.  Otherwise,
    we see if the remaining bit is less than the total under the first child.
    If so, go there, otherwise, it's the other child.  Then iterate.  Adding
    a node would probably involve placing higher weight nodes higher in
    the tree.  Currently I don't have a fast enough implementation of this
    for my purposes.  So for now I'm sticking to the mixture of lists &
    dictionaries.

    I believe this structure I'm describing is similar to a "partial sum tree"
    or a "Fenwick tree", but they seem subtly different from this.
    '''

    def __init__(self, weighted=False):
        self.item_to_position = {}
        self.items = []

        self.weighted = weighted
        if self.weighted:
            self.weight = defaultdict(int)  # presume all weights positive
            self.max_weight = 0
            self._total_weight = 0
            self.max_weight_count = 0

    def __len__(self):
        return len(self.items)

    def __contains__(self, item):
        return item in self.item_to_position

    def _update_max_weight(self):
        C = Counter(self.weight.values())  # may be a faster way to do this, we only need to count the max.
        self.max_weight = max(C.keys())
        self.max_weight_count = C[self.max_weight]

    def insert(self, item, weight=None):
        r'''
        If not present, then inserts the thing (with weight if appropriate)
        if already there, replaces the weight unless weight is 0

        If weight is 0, then it removes the item and doesn't replace.

        WARNING:
            replaces weight if already present, does not increment weight.


        '''
        if self.__contains__(item):
            self.remove(item)
        if weight != 0:
            self.update(item, weight_increment=weight)

    def update(self, item, weight_increment=None):
        r'''
        If not present, then inserts the thing (with weight if appropriate)
        if already there, increments weight

        WARNING:
            increments weight if already present, cannot overwrite weight.
        '''
        if weight_increment is not None:  # will break if passing a weight to unweighted case
            if weight_increment > 0 or self.weight[item] != self.max_weight:
                self.weight[item] = self.weight[item] + weight_increment
                self._total_weight += weight_increment
                if self.weight[item] > self.max_weight:
                    self.max_weight_count = 1
                    self.max_weight = self.weight[item]
                elif self.weight[item] == self.max_weight:
                    self.max_weight_count += 1
            else:  # it's a negative increment and was at max
                self.max_weight_count -= 1
                self.weight[item] = self.weight[item] + weight_increment
                self._total_weight += weight_increment
                self.max_weight_count -= 1
                if self.max_weight_count == 0:
                    self._update_max_weight
        elif self.weighted:
            raise Exception('if weighted, must assign weight_increment')

        if item in self:  # we've already got it, do nothing else
            return
        self.items.append(item)
        self.item_to_position[item] = len(self.items) - 1

    def remove(self, choice):
        position = self.item_to_position.pop(choice)
        last_item = self.items.pop()
        if position != len(self.items):
            self.items[position] = last_item
            self.item_to_position[last_item] = position

        if self.weighted:
            weight = self.weight.pop(choice)
            self._total_weight -= weight
            if weight == self.max_weight:
                # if we find ourselves in this case often
                # it may be better just to let max_weight be the
                # largest weight *ever* encountered, even if all remaining weights are less
                #
                self.max_weight_count -= 1
                if self.max_weight_count == 0 and len(self) > 0:
                    self._update_max_weight()

    def choose_random(self):
        # r'''chooses a random node.  If there is a weight, it will use rejection
        # sampling to choose a random node until it succeeds'''
        if self.weighted:
            while True:
                choice = random.choice(self.items)
                if random.random() < self.weight[choice] / self.max_weight:
                    break
            # r = random.random()*self.total_weight
            # for item in self.items:
            #     r-= self.weight[item]
            #     if r<0:
            #         break
            return choice

        else:
            return random.choice(self.items)

    def random_removal(self):
        r'''uses other class methods to choose and then remove a random node'''
        choice = self.choose_random()
        self.remove(choice)
        return choice

    def total_weight(self):
        if self.weighted:
            return self._total_weight
        else:
            return len(self)

    def update_total_weight(self):
        self._total_weight = sum(self.weight[item] for item in self.items)


def Gillespie_complex_contagion(G, rate_function, transition_choice,
                                get_influence_set, IC, return_statuses, tmin = 0, tmax=100, parameters = None,
                                return_full_data = False, sim_kwargs = None):

    r'''
    Initially intended for a complex contagion.  However, this can allow influence
    from any nodes, not just immediate neighbors.

    The complex contagion must be something that all nodes do something simultaneously

    **This is not the same as if node** ``v`` **primes node** ``u`` **and
    later node** ``w`` **causes** ``u`` **to transition.  This will require
    that both** ``v`` **and** ``w`` **have the relevant states at the moment**
    **of transition and it has forgotten any previous history.**

    :Arguments:

    **G** (NetworkX Graph)
        The underlying network

    **rate_function**
        A function that will take the network, a node, and the statuses of all
        the nodes and calculate the rate at which the node changes its status.
        The function is called like

        if parameters is None:
            rate_function(G, node, status)
        else:
            rate_function(G, node, status, parameters)

        where G is the graph, node is the node, status is a dict such that
        status[u] returns the status of u, and parameters is the parameters
        passed to the function.

        it returns a number, the combined rate at which ``node`` might change
        status.


    **transition_choice**
        A function that takes the network, a node, and the statuses of all the
        nodes and chooses which event will happen.  The function should be
        called [with or without ``parameters``]

        if ``parameters`` is ``None``:
            ``transition_choice(G, node, status)``
        else:
            ``transition_choice(G, node, status, parameters)``

        where ``G`` is the graph, ``node` is the node, ``status` is a dict
        such that ``status[u]`` returns the status of u, and ``parameters`` is
        the parameters passed to the function.

        It should return the new status of ``node`` based on the fact that the
        node is changing status.

    **get_influence_set**
        When a node ``u`` changes status, we want to know which nodes may have their
        rate altered.  We need to update their rates.  This function returns all
        nodes that may be affected by ``u`` (either in its previous state or its
        current state).  We will go through and recalculate the rates for all
        of these nodes.  For a contagion, we can simply choose all neighbors,
        but it may be faster to leave out any nodes that it wouldn't have
        affected before or after its transition (e.g., R or I neighbors in SIR).

        if ``parameters`` is None:
            ``get_influence_set(G, node, status)``
        else:
            ``get_influence_set(G, node, status, parameters)``

        where G is the graph, node is the node, status is a dict such that
        status[u] returns the status of u, and parameters is the parameters
        passed to the function.

        it should return the set of nodes whose rates need to be recalculated.

        Most likely, it is

        def get_influence_set(G, node, status):
            return G.neighbors(node)


    **IC**
        A dict.  IC[node] returns the status of node.

    **return_statuses** list or other iterable (but not a generator)
        The statuses that we will return information for, in the order
        we will return them.

    **tmin** number (default 0)
        starting time

    **tmax** number (default 100)
        stop time

    **return_full_data** boolean
        currently needs to be False.  True raises an error.

    **parameters**  list/tuple.
        Any parameters of the functions rate_function, transition_choice, influence_set
        We assume all three functions can accept parameters.
        Examples: recovery rate, transmission rate, ...


    :Returns:
    **(times, status1, status2, ...)**  tuple of numpy arrays
        first entry is the times at which events happen.
        second (etc) entry is an array with the same number of entries as ``times``
        giving the number of nodes of status ordered as they are in ``return_statuses``

    :SAMPLE USE:
    This simply does an SIR epidemic, by saying that the rate of becoming
    infected is tau times the number of infected neighbors.

    ::
        import networkx as nx
        import EoN
        import matplotlib.pyplot as plt
        from collections import defaultdict #makes defining the initial condition easier


        def rate_function(G, node, status, parameters):
            #This function needs to return the rate at which node changes status.
            #
            tau,gamma = parameters
            if status[node] == 'I':
                return gamma
            elif status[node] == 'S':
                return tau*len([nbr for nbr in G.neighbors(node) if status[nbr] == 'I'])
            else:
                return 0

        def transition_choice(G, node, status, parameters):
            #this function needs to return the new status of node.  We already
            #know it is changing status.
            #
            #this function could be more elaborate if there were different
            #possible transitions that could happen.
            if status[node] == 'I':
                return 'R'
            elif status[node] == 'S':
                return 'I'

        def get_influence_set(G, node, status, parameters):
            #this function needs to return any node whose rates might change
            #because ``node`` has just changed status.
            #
            #the only neighbors a node might affect are the susceptible ones.

            return {nbr for nbr in G.neighbors(node) if status[nbr] == 'S'}

        G = nx.fast_gnp_random_graph(100000,0.00005)

        gamma = 1.
        tau = 0.5
        parameters = (tau, gamma)

        IC = defaultdict(lambda: 'S')
        for node in range(200):
            IC[node] = 'I'

        t, S, I, R = EoN.Gillespie_complex_contagion(G, rate_function,
                                   transition_choice, get_influence_set, IC,
                                   return_statuses=('S', 'I', 'R'),
                                   parameters=parameters)

        plt.plot(t, I)

    '''

    if parameters is None:
        parameters = ()


    status = {node: IC[node] for node in G.nodes()}

    if return_full_data:
        node_history = {node :([tmin], [status[node]]) for node in G.nodes()}

    times = [tmin]
    t = tmin
    data = {}
    C = Counter(status.values())
    for return_status in return_statuses:
        data[return_status] = [C[return_status]]

    nodes_by_rate = _ListDict_(weighted=True)

    for u in G.nodes():
        rate = rate_function(G, u, status, parameters) # rate is specified by a function (initialization)!
        if rate >0:
            nodes_by_rate.insert(u, weight = rate)

    if nodes_by_rate.total_weight( ) >0:
        delay = random.expovariate(nodes_by_rate.total_weight())
    else:
        delay = float('Inf')
    t += delay
    attitude = [0.0]
    i=0
    b=False
    while nodes_by_rate.total_weight( ) > 0 and t< tmax:
        print('===== Time:',t)
        if b==True:#i % 200 == 0: # or 200 % t == 0 # 50 is good when you use neighbors for contagion
            print('Update nodes beliefs')
            statuses = ['S', 'I', 'R']
            d = {}
            Counts = Counter(status.values()) # Get S,I,R counts. It gives only S,I as R at least at the beginning is
            # empty
            for return_status in statuses:
                d[return_status] = [Counts[return_status]]
            for node in G:
                inf_neighs = len([nbr for nbr in G.neighbors(node) if status[nbr] == 'I']) #/ len([nbr for nbr in G.neighbors(node)])
                # new_belief = np.minimum(1.0, G.nodes[node]['attitude'].run_period(d['S'][0], d['I'][0], d['R'][0]))
                new_belief = G.nodes[node]['attitude'].run_period(inf_neighs)
                G.nodes[node]['masked'] = new_belief # Belief can't go beyond 1.0
        i+=1
        #avg_att = np.mean([G.nodes[nbr]['masked'] for nbr in G]) # takes too much time cauz it runs over all nodes!
        #attitude.append(avg_att)#G.nodes[46,46]['masked'])
        # print(avg_att)
        times.append(t)
        #        print(delta_t, nodes_by_rate.total_weight())
        node = nodes_by_rate.choose_random()
        new_status = transition_choice(G, node, status, parameters)

        for x in data.keys():
            data[x].append(data[x][-1])
        if status[node] in return_statuses:
            data[status[node]][-1] -= 1
        if new_status in return_statuses:
            data[new_status][-1] += 1

        status[node] = new_status
        if return_full_data:
            node_history[node][0].append(t)
            node_history[node][1].append(new_status)

        # update self
        weight = rate_function(G, node, status, parameters) # rate update!
        nodes_by_rate.insert(node, weight=weight)

        influence_set = get_influence_set(G, node, status, parameters)

        for nbr in influence_set:
            weight = rate_function(G, nbr, status, parameters) # rate update for the nodes that need to get updated!
            nodes_by_rate.insert(nbr, weight=weight)

        if nodes_by_rate.total_weight() > 0:
            delay = random.expovariate(nodes_by_rate.total_weight())
        else:
            delay = float('Inf')
        t += delay

    # plt.plot(np.array(times), attitude)
    # plt.show()
    print(len(times))
    if not return_full_data:
        returnval = []
        times = np.array(times)
        returnval.append(times)
        for return_status in return_statuses:
            data[return_status] = np.array(data[return_status])
            returnval.append(data[return_status])
        return returnval
    else:
        if sim_kwargs is None:
            sim_kwargs = {}
        return EoN.Simulation_Investigation(G, node_history, possible_statuses=return_statuses, **sim_kwargs)


@click.command()
@click.option("--periods", "-p", default=100, help="number of time periods to simulate")
@click.option("--samples", "-s", default=100, help="number of sample simulations to average together")
@click.option("--wear", "-w", default=1, help="strength of mask wearing")
@click.option("--dont-wear", "-d", default=1, help="strength of mask non-wearing")
@click.option("--fear", "-f", default=1, help="strength of fear")
@click.option("--hope", "-h", default=1, help="strength of hope")
@click.option("--r0", default=2.0, help="the r0 parameter of the model")
@click.option("--mask-factor", default=2.0, help="the mask_factor parameter of the model")
@click.option("--rate-factor", default=2.0, help="the rate factor parameter of the model")
@click.option("--susceptible", default=DEFAULT_SUSCEPTIBLE,
              help="the number of susceptible individuals in the initial population")
@click.option("--infectious", default=DEFAULT_INFECTIOUS,
              help="the number of infectious individuals in the initial population")
@click.option("--recovered", default=DEFAULT_RECOVERED,
              help="the number of recovered individuals in the initial population")
@click.option("--noise", default=DEFAULT_NOISE, help="the ACT-R noise parameter")
@click.option("--decay", default=pyactup.DEFAULT_DECAY, help="the ACT-R decay parameter")
@click.option("--temperature", default=DEFAULT_TEMPERATUE,
              help="the ACT-R blending temperature parameter")
@click.option("--mismatch", default=DEFAULT_MISMATCH, help="the ACT-R mismatch penalty")
@click.option("--optimized-learning", is_flag=True, help="use ACT-R optimized learning")
@click.option("--debug", "-D", is_flag=True, help="show debugging information")

def main(**kwargs):
    # :SAMPLE USE:
    # This simply does an SIR epidemic, by saying that the rate of becoming
    # infected is tau times the number of infected neighbors.

    def rate_function(G, node, status, parameters): # It runs initially with init conditions and then in the sim loop
        # This function needs to return the rate at which node changes status.
        #
        tau, gamma = parameters
        if status[node] == 'I': # I --> R
            return gamma
        elif status[node] == 'S': # S --> I
            # Let's say that the rate of someone becoming infected is (1-action)*tau, action=action(rate),
            # rate from counts/ You can use Don's sir(mapping_rt(action), S,I,R,N)
            # if node==(46,46):
            # print(node)
            # return_statuses = ['S', 'I', 'R']
            # d = {}
            # C = Counter(status.values()) # Get S,I,R counts. It gives only S,I as R at least at the beginning is empty
            # for return_status in return_statuses:
            #     d[return_status] = [C[return_status]]

            # for key,val in d.items():
            #     exec(key + '=val') # converts the dictionary into separate variables
            # accessing state (SIR counts from state class) for debug: G.nodes[0,0]['attitude'].state

            # inf_neighs = len([nbr for nbr in G.neighbors(node) if status[nbr] == 'I']) #/ len([nbr for nbr in G.neighbors(node)])
            # # new_belief = np.minimum(1.0, G.nodes[node]['attitude'].run_period(d['S'][0], d['I'][0], d['R'][0]))
            # new_belief = G.nodes[node]['attitude'].run_period(d['S'][0], d['I'][0], d['R'][0], inf_neighs)
            # G.nodes[node]['masked'] = new_belief # Belief can't go beyond 1.0

            # print('Ok')
            # WITH r IT RUNS WAY FASTER!!!
            r = len([nbr for nbr in G.neighbors(node) if status[nbr] == 'I']) # If you dont use this there are going
            # to be spontaneous Infections (nodes that do not have around them any Infected neighbor)
            # print(r)
            # return tau * (1 - new_belief)#* r # by using the neighbor
            # calculation here it keeps the number S constant. This is because only the I are converted to R and no S
            # to I
            return tau * (1 - G.nodes[node]['masked'])*r # Take out r so you can have spontaneous outbreaks and check
            # the mean attitude
        else:
            return 0

    def transition_choice(G, node, status, parameters):
        # this function needs to return the new status of node.  We already
        # know it is changing status.
        #
        # this function could be more elaborate if there were different
        # possible transitions that could happen.
        # HERE WE NEED TO UPDATE BELIEFS!!! A S node will be visited twice (S->I->R)
        # NO WE NEED TO UPDATE BELIEFS AT EVERY TIMESTEP IN GILLESPIE
        # print(node)
        if status[node] == 'I':
            return 'R'
        elif status[node] == 'S':
            return 'I'

    def get_influence_set(G, node, status, parameters):
        # this function needs to return any node whose rates might change
        # because ``node`` has just changed status.
        #
        # the only neighbors a node might affect are the susceptible ones.

        return {nbr for nbr in G.neighbors(node) if status[nbr] == 'S'}

    now = datetime.now()
    start_time = now.strftime("%H:%M:%S")
    print("Time Start Simulation =", start_time)
    N = 100#int(np.sqrt(DEFAULT_INFECTIOUS + DEFAULT_SUSCEPTIBLE))
    tmax = 150
    G = nx.grid_2d_graph(N, N)  # each node is (u,v) where 0<=u,v<=99, SUPER FAST SIMULATION
    # INFECTIONS ARE PLACED IN THE MIDDLE!
    init_infected = [(u, v) for (u, v) in G if 45 < u < 55 and 45 < v < 55]
    masks = {node: 0.0 for node in G}
    nx.set_node_attributes(G, values=masks, name='masked')

    mw = MaskWearer(wear=kwargs["wear"],
                    dont_wear=kwargs["dont_wear"],
                    fear=kwargs["fear"],
                    hope=kwargs["hope"],
                    r0=kwargs["r0"],
                    mask_factor=kwargs["mask_factor"],
                    rate_factor=kwargs["rate_factor"],
                    susceptible=kwargs["susceptible"],
                    infectious=kwargs["infectious"],
                    recovered=kwargs["recovered"],
                    noise=kwargs["noise"],
                    decay=kwargs["decay"],
                    temperature=kwargs["temperature"],
                    mismatch=kwargs["mismatch"],
                    optimized_learning=kwargs["optimized_learning"])
    mw = MaskWearer()
    # for node in G:
    #     G.nodes[node]['attitude'] = mw
    gamma = 0.04#1.
    tau = 0.4 #beta
    parameters = (tau, gamma)

    IC = defaultdict(lambda: 'S')
    for node in init_infected:
        IC[node] = 'I' # for N=100 and 45<x,y<55 there are 81 nodes initial Infected

    # ANIMATION
    color_dict = {'S': '#009a80', 'I': '#ff2000', 'R': 'gray'}  # ,'Vac': '#5AB3E6'}
    pos = {node: node for node in G}
    tex = False
    sim_kwargs = {'color_dict': color_dict, 'pos': pos}
                  #'tex': tex}  # sim doesnt work if we include these. By default SIR is assumed.

    sim = Gillespie_complex_contagion(G, rate_function, # use EoN.Gillespie... to use the original function
                                                 transition_choice, get_influence_set, IC, return_full_data=True,
                                                 tmax = tmax,
                                                 sim_kwargs=sim_kwargs,
                                                 return_statuses=('S', 'I', 'R'),
                                                 parameters=parameters)


    # Simulation ended
    end = datetime.now()
    finish_time = end.strftime("%H:%M:%S")
    print("Time Finish Simulation =", finish_time)
    times, D = sim.summary()
    newD = {'S': D['S'], 'I': D['I'], 'R': D['R']}

    new_timeseries = (times, newD)
    sim.add_timeseries(new_timeseries, color_dict={'S': '#009a80', 'I': '#ff2020', 'R': 'gray'})

    nx_kwargs = dict(edgecolors='black', node_size=4, width=0.0)
    print('Creating Video')
    ani = sim.animate(ts_plots=[['S', 'I', 'R']], **nx_kwargs)
    ani.save('01.mp4', fps=5, extra_args=['-vcodec', 'libx264'])
    print('Video Saved')
    # NO ANIMATION
    # t, S, I, R = Gillespie_complex_contagion(G, rate_function, # use EoN.Gillespie... to use the original function
    #                                              transition_choice, get_influence_set, IC, tmax = tmax,
    #                                              return_statuses=('S', 'I', 'R'),
    #                                              parameters=parameters)
    #
    # # Simulation ended
    # end = datetime.now()
    # finish_time = end.strftime("%H:%M:%S")
    # print("Time Finish Simulation =", finish_time)
    # #TODO: Use code below and iterate over t so you will get the avg attitude towards the pandemic
    # # print('Final masked probability:', np.mean([sim.G.nodes[nbr]['masked'] for nbr in sim.G]))
    #
    # plt.plot(t, S, label='Susceptible')
    # plt.plot(t, I, label='Infected')
    # plt.plot(t, R, label='Recovered')
    # plt.legend()
    # plt.show()

if __name__ == "__main__":
    main()

# NOTES:
# The way it is you can just run IBL multiple times with the rate of one timestep and put this value to the masked
# attribute. We do it like this in case we run interactive stuff and input is a neighboring property.