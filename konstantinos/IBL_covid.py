import click
from collections import defaultdict
import dataclasses
from itertools import count
import logging
import math
import matplotlib.pyplot as plt
from prettytable import PrettyTable
import pyactup
from tqdm import tqdm


DEFAULT_SUSCEPTIBLE = 999_000
DEFAULT_INFECTIOUS = 1_000
DEFAULT_RECOVERED = 0
DEFAULT_NOISE = 0.1
DEFAULT_TEMPERATURE = 1.0
DEFAULT_MISMATCH = 1.0


@dataclasses.dataclass
class State:
    susceptible: int = DEFAULT_SUSCEPTIBLE
    infectious: int = DEFAULT_INFECTIOUS
    recovered: int = DEFAULT_RECOVERED

    @property
    def population(self):
        return self.susceptible + self.infectious + self.recovered

    @property
    def infection_rate(self):
        return self.infectious / self.population

    def sir(self, r0=1.0, periods=1):
        logging.debug("sir called: %s %s", self, r0)
        for i in range(periods):
            rt = r0 * self.susceptible / self.population
            print('S:', self.susceptible, 'Rt:', rt)
            # The new S,I,R populations
            self.recovered += self.infectious
            self.infectious = min(math.floor(self.infectious * rt), self.susceptible)
            self.susceptible -= self.infectious
        else:
            rt = r0
        logging.debug("sir result = %s", rt)
        return rt

def mask_rt_mapping(probability, r0=2.0, mask_factor=2.0):
    return math.pow(mask_factor, mask_factor * (0.5 - probability)) * r0 / mask_factor

SIMILARITY_RANGE = 2

def linear_similarity(x, y):
    if x is None or y is None:
        return 1
    else:
        result = 1 - abs(y - x) / SIMILARITY_RANGE
        assert result >= 0 and result <= 1
        return result

pyactup.set_similarity_function(linear_similarity, "input")


class MaskWearer(pyactup.Memory):

    def __init__(self,
                 wear=1, dont_wear=1, fear=1, hope=1,
                 r0=2.0, mask_factor=2.0, rate_factor=0.5,
                 susceptible=DEFAULT_SUSCEPTIBLE,
                 infectious=DEFAULT_INFECTIOUS,
                 recovered=DEFAULT_RECOVERED,
                 **mem_params):
        if "noise" not in mem_params:
            mem_params["noise"] = DEFAULT_NOISE
        if "temperature" not in mem_params:
            mem_params["temperature"] = DEFAULT_TEMPERATURE
        if "mismatch" not in mem_params:
            mem_params["mismatch"] = DEFAULT_MISMATCH
        super().__init__(**mem_params)
        self.learning_time_increment = 0
        self.retrieval_time_increment = 1
        self.state = State(susceptible, infectious, recovered)
        self.r0 = r0
        self.rate = r0
        self.mask_factor = mask_factor
        self.rate_factor = rate_factor
        for n, i, a in ((wear, None, 1.0), # put the extreme values
                        (dont_wear, None, 0.0),
                        (fear, 2.0, 1.0),
                        (hope, 0.0, 0.0)):
            for j in range(n):
                self.learn(input=i, action=a)
        self.activation_history = [] # enable it here so we can use it. Import pprint and use it for better printing.
        logging.debug("initialized %s (%s, %d, %d, %d, %d)",
                      self, self.state, wear, dont_wear, fear, hope)

    def mask_rt_mapping(self, probability):
        return (math.pow(self.mask_factor, self.mask_factor * (0.5 - probability)) # (r0/mf) * mf^( mf*(.5-prob) )
                * self.r0 / self.mask_factor)

    def run_period(self):
        if self.rate <= 0:
            return
        assert self.rate <= SIMILARITY_RANGE
        action = self.blend("action", input=self.rate)
        result = dataclasses.asdict(self.state) # the S,I,R from state function are becoming a dictionary
        result["rate"] = self.rate
        new_rate = self.state.sir(mask_rt_mapping(action)) # S,I,R populations are getting updated here
        outcome = (self.state.infection_rate - new_rate) / new_rate
        self.rate = self.rate_factor * self.rate + (1.0 - self.rate_factor) * new_rate
        logging.debug("ran period on %s (%f) => %s", self, self.rate, result)
        return result


@click.command()
@click.option("--periods", "-p", default=300, help="number of time periods to simulate")
@click.option("--samples", "-s", default=1, help="number of sample simulations to average together")
@click.option("--wear", "-w", default=1, help="strength of mask wearing")
@click.option("--dont-wear", "-d", default=1, help="strength of mask non-wearing")
@click.option("--fear", "-f", default=1, help="strength of fear")
@click.option("--hope", "-h", default=1, help="strength of hope")
@click.option("--r0", default=2.0, help="the r0 parameter of the model")
@click.option("--mask-factor", default=2.0, help="the mask_factor parameter of the model")
@click.option("--rate-factor", default=2.0, help="the rate factor parameter of the model")
@click.option("--susceptible", default=DEFAULT_SUSCEPTIBLE,
              help="the number of susceptible individuals in the initial population")
@click.option("--infectious", default=DEFAULT_INFECTIOUS,
              help="the number of infectious individuals in the initial population")
@click.option("--recovered", default=DEFAULT_RECOVERED,
              help="the number of recovered individuals in the initial population")
@click.option("--noise", default=DEFAULT_NOISE, help="the ACT-R noise parameter")
@click.option("--decay", default=pyactup.DEFAULT_DECAY, help="the ACT-R decay parameter")
@click.option("--temperature", default=DEFAULT_TEMPERATURE,
              help="the ACT-R blending temperature parameter")
@click.option("--mismatch", default=DEFAULT_MISMATCH, help="the ACT-R mismatch penalty")
@click.option("--optimized-learning", is_flag=True, help="use ACT-R optimized learning")
@click.option("--debug", "-D", is_flag=True, help="show debugging information")
def main(**kwargs):
    if kwargs["debug"]:
        logging.getLogger().setLevel(logging.DEBUG)
        logging.info("Debugging enabled")
    logging.debug("main called with arguments %s", kwargs)
    results = [defaultdict(float) for i in range(kwargs["periods"])]
    for s in tqdm(range(kwargs["samples"])): # each percentage is a complete SIR simulation (there is stochasticity
        # so we do multiple runs)
        mw = MaskWearer(wear=kwargs["wear"],
                        dont_wear=kwargs["dont_wear"],
                        fear=kwargs["fear"],
                        hope=kwargs["hope"],
                        r0=kwargs["r0"],
                        mask_factor=kwargs["mask_factor"],
                        rate_factor=kwargs["rate_factor"],
                        susceptible=kwargs["susceptible"],
                        infectious=kwargs["infectious"],
                        recovered=kwargs["recovered"],
                        noise=kwargs["noise"],
                        decay=kwargs["decay"],
                        temperature=kwargs["temperature"],
                        mismatch=kwargs["mismatch"],
                        optimized_learning=kwargs["optimized_learning"])
        mw = MaskWearer()
        for p in range(kwargs["periods"]): # run for 'period' "days". We run the SIR for a number of time units.
            r = mw.run_period() # just one time step of the simulaiton
            for k in ("rate", "susceptible", "infectious", "recovered"):
                #Notes: Infectious seems to be cumulative and remains constant. It should be the counts at time t.
                results[p][k] += r[k] / kwargs["samples"] # Here he averages over the multiple runs for that timestep
                # (period)
    tab = PrettyTable(["period", "rt", "susceptible", "infectious", "recovered"])
    for r, i in zip(results, count(1)):
        tab.add_row([i, f"{r['rate']:.2f}", round(r["susceptible"]), round(r["infectious"]), round(r["recovered"])])
    print(tab)
    fig, axs = plt.subplots(3, sharex=True)
    fig.set_figheight(8)
    fig.suptitle(f"infections and rate by period")
    axs[0].plot(range(1, kwargs["periods"] + 1), [r["rate"] for r in results])
    axs[0].set_title("rt")
    axs[0].set_ylim([0, SIMILARITY_RANGE])
    axs[1].plot(range(1, kwargs["periods"] + 1), [r["infectious"] for r in results])
    axs[1].set_title("current infections")
    axs[2].plot(range(1, kwargs["periods"] + 1),
                [r["infectious"] + r["recovered"] for r in results])
    axs[2].set_title("cumulative infections")
    plt.show()


if __name__ == "__main__":
    main()
