(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(:alexandria :iterate :cl-ppcre :sqlite :bordeaux-threads :uiop :cl-threadpool :verbose)
                :silent (not *load-verbose*)))

(defpackage :covid
  (:use common-lisp :alexandria :iterate)
  (:local-nicknames (#:s :sqlite) (:u :uiop) (:tp :cl-threadpool))
  (:export #:run-simulations))

(in-package :covid)

(load (merge-pathnames "act-up-v1_2" *load-pathname*))

(defparameter +minimum-infection-probability+ 0.2)
(defparameter +maximum-infection-probability+ 0.95)
(defparameter +recovery-time+ 21)

(defparameter +default-root+ #P"/home/mkm/covid/")
(defparameter +default-thread-count+ 4)
(defparameter +default-rounds+ 100)
(defparameter +fast-directory+ #P"/dev/shm/")
(defparameter  +truncate-neighbors+ 12)

(defun core-count ()
  ;; Returns nil if it can't figure it out. Currently probably only works on Linux.
  (values
   (ignore-errors
    (ppcre:register-groups-bind (s)
        ("cpu cores\\s+:\\s*(\\d+)\\s*$" (u:run-program "grep 'cpu cores' /proc/cpuinfo" :output :string))
      (when s
        (parse-integer s))))))

(defparameter *default-thread-count* (or (core-count) +default-thread-count+))

(defvar *worker-count*)
(defvar *threadpool*)
(defvar *parameters*)

(setf tp:*logger* (lambda (level who fmt args)
                    (if (and (eq who :cl-threadpool) (eq level :info))
                        (apply #'v:debug who fmt args)
                        (apply #'v:log level who fmt args))))
(v:output-here)                         ; in case we're running under SLIME

(defun canonical-name (s &optional (keyword t))
  (funcall (if keyword #'make-keyword #'intern) (string-upcase (substitute #\- #\_ s))))

(proclaim '(ftype function print-node))

(defstruct (node (:print-object print-node))
  (id nil :read-only t)
  (neighbors nil)
  state
  fear
  mask
  sodist
  vax
  (memory (init-memory))
  (recovery-time nil))

(defun print-node (node &optional (stream *standard-output*))
  (print-unreadable-object (node stream :type t :identity t)
    (let ((nn (node-neighbors node)))
      (format stream "~D (~{~D~^ ~}~:[~; …~]) ~A ~@{~,2F~^ ~}"
              (node-id node)
              (map 'list #'node-id (subseq nn 0 (min (length nn) +truncate-neighbors+)))
              (> (length nn) +truncate-neighbors+)
              (node-state node)
              (node-fear node)
              (node-mask node)
              (node-sodist node)
              (node-vax node)))))

(defun read-network (db)
  (iter (with nodes := (make-array (1+ (s:execute-single db "SELECT MAX(id) FROM nodes")) :initial-element nil))
        (with *memory* := nil)          ; don't overwrite the global value, just in case
        (for (id st f m sd v) :in-sqlite-query "SELECT id, state, fear, mask, sodist, vax
                                                FROM nodes WHERE time = 0" :on-database db)
        (for n := (make-node :id id
                             :state (make-keyword (string-upcase st))
                             :fear f
                             :mask m
                             :sodist sd
                             :vax v))
        (when (eq (node-state n) :I)
          (setf (node-recovery-time n) (random +recovery-time+)))
        (setf (aref nodes id) n)
        (finally (iter (for (n1 n2) :in-sqlite-query "SELECT node_1, node_2 FROM edges" :on-database db)
                       (pushnew (aref nodes n2) (node-neighbors (aref nodes n1)))
                       (pushnew (aref nodes n1) (node-neighbors (aref nodes n2))))
                 (setf nodes (shuffle (delete nil nodes)))
                 (iter (for n :in-vector nodes)
                       (setf (node-neighbors n) (sort (coerce (node-neighbors n) 'vector) #'<
                                                      :key #'node-id)))
                 (return nodes))))

(defun write-node (db node)
  (s:execute-non-query db "INSERT INTO nodes VALUES(?, ?, ?, ?, ?, ?, ?)"
                       (node-id node)
                       (actr-time)
                       (string (node-state node))
                       (node-fear node)
                       (node-mask node)
                       (node-sodist node)
                       (node-vax node)))

(defun parameter-value (name &optional (conversion :string))
  ;; Note that there is no way to distinguish a false Boolean or nil symbol from a missing
  ;; value. This seems unlikely to be a problem in practice, though.
  (when-let ((plist (gethash (make-keyword (string-upcase name)) *parameters*)))
    (or (getf plist conversion)
        (setf (getf plist conversion)
              (ecase conversion
                (:keyword (canonical-name (getf plist :string)))
                (:symbol (intern (canonical-name (getf plist :string) nil)))
                (:integer (parse-integer (getf plist :string)))
                (:float (let ((result (read-from-string (getf plist :string))))
                          (coerce result 'float)))
                (:boolean (let ((result (read-from-string (getf plist :string))))
                            (not (or (null result)
                                     (and (numberp result) (zerop result))
                                     (member result '("false" "no" "null" "zero")
                                             :test #'string-equal))))))))))

(defmacro with-parameters ((database) &body body)
  `(%with-parameters ,database (lambda () ,@body)))

(defun %with-parameters (database thunk)
  (s:with-open-database (db database)
    (let ((*parameters* (make-hash-table :test 'eq)))
      (iter (for (name value) :in-sqlite-query "SELECT name, value FROM parameters" :on-database db)
            (setf (gethash (canonical-name name) *parameters*) (list :string value)))
      (labels ((memory-parameter (key name convert)
                 (when-let ((v (parameter-value name convert)))
                   (parameter key v))))
        (memory-parameter :bll :decay :float)
        (memory-parameter :mp :mismatch :float)
        (memory-parameter :ans :noise :float)
        (memory-parameter :rt :threshold :float)
        (memory-parameter :tmp :temperature :float)
        (memory-parameter :ol :optimized-learning :boolean))
      (funcall thunk))))

(defmacro with-network ((db-var node-var database) &body body)
  `(%with-network ,database (lambda (,db-var ,node-var) ,@body)))

(defun %with-network (database thunk)
  (s:with-open-database (db database)
    (funcall thunk db (read-network db))))

;; Bozo's model of infection. Just something to implement to demonstrate we can run
;; simulations. This needs to be replaced by something more realistic.

(defun learn-attitudes (node)
  (labels ((learn-one (n)
             ;; (format t "~2%~@{~S~%~}~2%" node (node-memory node) n (node-memory n))
             (learn `((fear ,(node-fear n))
                      (mask ,(node-mask n))
                      (sodist ,(node-sodist n))
                      (vax ,(node-vax n)))
                    :memory (node-memory node))))
    (learn-one node)
    (map nil #'learn-one (node-neighbors node)))
  node)

(defun update-attitudes (node)
  (macrolet ((update-one (attr fn)
               `(setf (,fn node) (blend nil ',attr (node-memory node)))))
    (update-one fear node-fear)
    (update-one mask node-mask)
    (update-one sodist node-sodist)
    (update-one vax node-vax))
  node)

(defun prepare-transitions (node)
  (ecase (node-state node)
    (:S (let ((inf (count-if (lambda (n) (eq (node-state n) :I)) (node-neighbors node))))
          (when (and (not (zerop inf))
                     (<= (random 1.0)
                         (* (/ (float inf) (length (node-neighbors node)))
                            (+ +minimum-infection-probability+
                               (* (/ (- 4.0 (+ (node-fear node)
                                               (node-mask node)
                                               (node-sodist node)
                                               (node-vax node)))
                                     8)
                                  (- +maximum-infection-probability+
                                     +minimum-infection-probability+))))))
            (setf (node-recovery-time node) (+ (actr-time) +recovery-time+))
            (list node))))
    (:I (when (>= (actr-time) (node-recovery-time node))
          (setf (node-recovery-time node) nil)
          (list node)))
    (:R)))

(defun execute-transition (node)
  ;; assumes node *should* transition
  (setf (node-state node) (or (getf '(:S :I :I :R) (node-state node))
                              (error "Unexpected state transition ~S" node))))

(defparameter *worker-lock* (bt:make-lock "nodes"))
(defparameter *unprocessed-node-count* -1)

(defun process-nodes (nodes fn)
  (iter (for i := (bt:with-lock-held (*worker-lock*) (decf *unprocessed-node-count*)))
        (while (>= i 0))
        (funcall fn (aref nodes i))))

(defun run-one-simulation (database rounds)
  (with-network (db nodes database)
    (labels ((run (fn)
               (setf *unprocessed-node-count* (length nodes))
               (tp:run-jobs *threadpool*
                            (make-sequence 'list *worker-count* :initial-element (curry #'process-nodes nodes fn)))))
      (dotimes (r rounds)
        (v:info :covid "Running round ~D on ~A" r (pathname-name database))
        (handler-case
            (progn
              (run #'learn-attitudes)
              (actr-time 1)
              (run #'update-attitudes)
              ;; The transisition stuff currently is so cheap it's not worth parallelizing.
              ;; Consider doing otherwise if things become more expensive.
              (iter (for node :in-vector nodes)
                    (nconcing (prepare-transitions node) :into transitioning-nodes)
                    (finally (mapc #'execute-transition transitioning-nodes)))
              ;; Note that writing to the DB cannot be parallelized.
              (s:with-transaction db
                (map nil (curry #'write-node db) nodes)))
          (error (e) (v:error :covid e))))
      (values (lambda () nodes) (length nodes) (actr-time)))))

(defun run-simulations (&key (root +default-root+) only (workers *default-thread-count*) rounds)
  (setf root (u:ensure-directory-pathname root))
  (setf only (iter (for thing :in (if (listp only) only (list only)))
                   (for name := (pathname-name thing))
                   (for type := (pathname-type thing))
                   (if (or (null type) (string= type "sqlite3"))
                       (collect name)
                       (v:warn :covid "Ignoring ~S as it has an unexpected file type" thing))))
  (unless (and (integerp workers) (> workers 0))
    (set workers 1))
  (let ((*threadpool* (tp:make-threadpool workers :name "COVID")))
    (v:info :covid "~D workers" workers)
    (unwind-protect
         (iter (with *worker-count* := workers)
               (with in-dir := (merge-pathnames #P"input/" root))
               (with out-dir := (merge-pathnames #P"output/" root))
               (for f :in (u:directory-files in-dir "*.sqlite3"))
               (handler-case
                   (when (or (null only) (member (pathname-name f) only :test #'string=))
                     (with-parameters (f)
                       (let ((reps (or (parameter-value :rep :integer) 1))
                             (db-rounds (or rounds (parameter-value :rounds :integer) +default-rounds+)))
                         (v:info :covid "Processing ~D reps of database ~A for ~D rounds"
                                 reps (pathname-name f) db-rounds)
                         (iter (for rep :from 0 :below reps)
                               (for file := (format nil "~A-~3,'0D.sqlite3" (pathname-name f) rep))
                               (for copy := (merge-pathnames file +fast-directory+))
                               (unwind-protect
                                    (progn
                                      (v:info :covid "Starting ~A" file)
                                      (u:run-program (format nil "/bin/cp ~A ~A" f copy))
                                      (run-one-simulation copy db-rounds)
                                      (u:run-program (format nil "/bin/mv ~A ~A" copy (merge-pathnames file out-dir))))
                                 (u:delete-file-if-exists copy))))
                       (counting t)))
                 (error (e) (v:error :covid e))))
      (tp:stop *threadpool*))))
